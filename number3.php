<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    function randomLiner($filename){

        if(file_exists($filename)){
            
            $newFile = strtolower($filename);
    
            $letterLength =  substr_count($newFile, 'a')
                            + substr_count($newFile, 'e')
                            + substr_count($newFile, 'i')
                            + substr_count($newFile, 'o')
                            + substr_count($newFile, 'u');
            
        
            $file = fopen($filename,'r');
    
            $line2 = 2;
    
            $line3 = 3;
            
            if($letterLength % 2 == 0){
                
                $count = 0;
    
                while(!feof($file)){
    
                    $count++;
    
                    $lines = fgets($file);
    
                    if($count == $line2){
    
                         return "3. ".$lines."<br>";
    
                    }
                }
            
            }
            else
            {
                
                $count = 0;
                 
                while(!feof($file)){
    
                    $count++;
    
                    $lines = fgets($file);
    
                    if($count == $line3){
    
                         return $lines;
    
                    }
                }
    
    
            }
    
             
    
    
        } 
        else
        {
            return sprintf('file %s does not exist !',$filename);
        }
    }
    echo randomLiner("aldrin.txt");
    
    ?>
</body>
</html>