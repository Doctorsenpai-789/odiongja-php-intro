<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Number1</title>
</head>
<body>
 


<?php

function fileLine ( $file,$lineNum) {
   $fp = fopen($file, 'r');
    $counter = 0;
    while (!feof($fp)) {
      $counter++;
      $line = fgets($fp);
      if ($counter == $lineNum){
        return $line;
      }
       
    }
    die("The file has fewer than ".$lineNum." lines!");
   
  }
  
 
  echo fileLine("text1.txt",2);
  
?>


</body>
</html>